#include "FileStream.h"
#include "Logger.h"
#include "OutStream.h"
#include "OutStreamEncrypted.h"
using namespace msl;
int main()
{
	OutStream stream;
	stream << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	FileStream a("t.txt");
	a << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	OutStreamEncrypted b(1);
	b << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	Logger log;
	log << "I am the Doctor and I'm " << 1500 << " years old" << endline;

	Logger log2;
	log2 << "hi";
	getchar();
}