#include "Logger.h"
using namespace msl;
//builders
Logger::Logger()
{
	_file = stdout;
	_startLine = false;
	this->os;
}
Logger::~Logger()
{
}

//operators
Logger& msl::operator<<(Logger& l, const char *str)
{
	l.setStartLine();
	l.os << str;
	return l;
}
Logger & msl::operator<<(Logger & l, int num)
{
	l.setStartLine();
	l.os << num;
	return l;
}
Logger & msl::operator<<(Logger & l, void(*pf)(FILE *fp))
{
	l.setStartLine();
	pf(l._file);
	l._startLine = false;
	return l;
}

//functions
void Logger::setStartLine()
{
	unsigned static int runs = 0;
	if (_startLine == false)
	{
		runs++;
		os << "LOG ";
		os << runs;
		_startLine = true;
	}
}
