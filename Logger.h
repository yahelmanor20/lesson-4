#pragma once
#include "OutStream.h"
namespace msl
{
	class Logger
	{
	public:
		OutStream os;
		bool _startLine;
		void setStartLine();
		FILE * _file;

		Logger();
		~Logger();
		friend Logger& operator<<(Logger& l, const char *str);
		friend Logger& operator<<(Logger& l, int num);
		friend Logger & operator<<(Logger & l, void(*pf)(FILE *fp));

	};
}