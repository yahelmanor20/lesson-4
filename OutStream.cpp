#include "OutStream.h"
using namespace msl;

OutStream::OutStream()
{
	_file = stdout;
}
OutStream::~OutStream()
{
}
OutStream& OutStream::operator<<(const char *str)
{
	fprintf(_file, "%s", str);
	return *this;
}
OutStream& OutStream::operator<<(int num)
{
	fprintf(_file, "%d", num);
	return *this;
}
OutStream& OutStream::operator<<(void(*pf)(FILE *fp))
{
	pf(_file);
	return *this;
}

void msl::endline(FILE * fp)
{
	fprintf(fp, "\n");
}
