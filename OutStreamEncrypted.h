#pragma once
#include "OutStream.h"
namespace msl
{
#define MAX_HIST 132
#define HIST 94
	class OutStreamEncrypted :public OutStream
	{
	public:
		OutStreamEncrypted(const int hist);
		~OutStreamEncrypted();
		OutStreamEncrypted & operator<<(const char * str);
		OutStreamEncrypted & operator<<(int num);
		OutStreamEncrypted & operator<<(void(*pf)(FILE *fp));
	private:
		int _hist;
	};
}