#include "OutStreamEncrypted.h"
using namespace msl;
OutStreamEncrypted::OutStreamEncrypted(const int hist)
{
	_file = stdout;
	_hist = hist;
}
OutStreamEncrypted::~OutStreamEncrypted()
{
}
//encript the str and  print char by char (if the final coded is too high minus it by 94)
OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	int hist = 0, i = 0;
	while (str[i] != NULL)
	{
		hist = _hist + int(str[i]);
		if (hist > MAX_HIST)
		{
			hist -= HIST;
		}
		fprintf(_file, "%c", char(hist));
		i++;
	}

	return *this;
}
//encript the num and  print int by int 
OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	int i = 0, hist = 0, tmp = 0;
	while (num != 0)
	{
		tmp = num % 10;
		hist = _hist + tmp;
		fprintf(_file, "%d", hist);
		num /= 10;
	}
	return *this;
}
OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE *fp))
{
	pf(_file);
	return *this;
}
